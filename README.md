# README #

## Public Repository ##

Welcome to the WLPC 2018 Python for Wi-Fi Engineers Bootcamp.  The files in here will be needed throughout the class.  You should already have the first couple lab documents, but they are included here for your reference.

### What is this repository for? ###

* TBD

### How do I get set up? ###

* TBD

### Do I Need an intel NUC ###

* No, any intel based linux machine should work
* Most of these packages should be available on other platforms (ARM)
* But none of that has been tested.

# If I want to run this on an RPI, what do i need? ###
* The base image has Ubuntu 16.04.3 LTS
* OpenSSH is running by default
* VNC (Purely for running headless) is install and running by default
* A user account pyfi is auto-logged in by default

### Who do I talk to? ###
* Jake Snyder
	* [@jsnyder81](https://twittter.com/jsynder81)
	* [jsnyder81@gmail.com](mailto:jsnyder81@gmail.com)
	* Bitbucket account: @jsnyder81
* Ryan M. Adzima
	* [@radzima](https://twittter.com/radzima)
	* [ryan.m.adzima@gmail.com](mailto:ryan.m.adzima@gmail.com)
	* Bitbucket account: @radzima