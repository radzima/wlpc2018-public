#!/usr/bin/env python
# paramiko_example.py - simple example of using paramiko
# version: 0.1
# Author: jsnyder81

import paramiko
import getpass

username = input("Username:")
password = getpass.getpass("Password")

sshClient = paramiko.SSHClient()
sshClient.set_missing_host_key_policy(paramiko.AutoAddPolicy())
sshClient.connect("127.0.0.1", username="pyfi", password="wlpc2018")
stdin, stdout, stderr = sshClient.exec_command("dir")
stdin.close()
outlines=stdout.readlines()
my_response = ''.join(outlines)
print(my_response)
