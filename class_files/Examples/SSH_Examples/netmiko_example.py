#!/usr/bin/env python
# netmiko_example.py - simple example of using netmiko
# version: 0.1
# Author: jsnyder81
import netmiko
import getpass

netClient = netmiko.ConnectHandler(device_type='cisco_ios', ip=input("IP Address: "), username=input("Username: "), password=getpass.getpass("Password: "))
netClient.find_prompt()
output = netClient.send_command("show run")
print(output)
