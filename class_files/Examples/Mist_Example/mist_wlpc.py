#!/usr/bin/python
import requests
import json
import sys
import getpass
import jinja2

mist_base_url = "api.mist.com"
csrftoken = ""
mist_auth_token = ""
#mist_auth_token = "houHfeCL9u1eMjFkXeBRrQqHr8lW4fFWvn3bnlHxUDC6x7zcj2wggnxhqS1aYN9U2tAi5pKhdUORD5o8G3c49ru9Bf2Jkl41"

mist_email = ""
mist_password = ""

class bandTemplate(object):
    band = ""
    disabled = ""
    channels = ""
    bandwidth = ""
    power_dbm = ""
    def __init__(self, myBand, myBandwidth, myChannels, myPowerdbm, bandDisabled):
        self.band = myBand
        self.bandwidth = myBandwidth
        self.channels = myChannels
        self.power_dbm = myPowerdbm
        self.disabld = bandDisabled

def mist_CheckIfLoggedIn(mist_session):
     print("Checking if logged in via token")
     api_call = "/api/v1/self"
     request_url = "https://" + mist_base_url + api_call
     header = {"content-type": "application/json"}
     response = mist_session.get(request_url, headers=header, verify=False)
     r_json = response.json()
     print("Code: " + str(response.status_code))
     print(str(r_json))
     if response.status_code == 200:
         return True
     else:
         return False

def mist_CheckIfLoggedInToken(token_id):
    print("Checking if logged in via token")
    api_call = "/api/v1/self"
    req_url = "https://" + mist_base_url + api_call
    header = {"content-type": "application/json", 'Authorization': 'token {}'.format(token_id)}
    mist_session = requests.Session()
    response = mist_session.get(req_url, headers=header, verify=False)
    r_json = response.json()
    if response.status_code == 200:
        print("API Authenticated")
        return True
    else:
        print("Code: " + str(response.status_code))
        print(str(r_json))
        return False

def mist_LogInCredentials(mist_session):
    print("Logging in")
    mist_LogInCredentials_url = "/api/v1/login"
    req_url = "https://" + mist_base_url + mist_LogInCredentials_url #https://api.mist.com/api/v1/login
    mist_email = input("enter your mist email:")
    mist_password = getpass.getpass()
    payload = {"email": mist_email, "password": mist_password}
    header = {"content-type": "application/json"}
    response = mist_session.post(req_url, data=json.dumps(payload), headers=header, verify=False)
    r_json = response.json()
    csrftoken = mist_session.cookies['csrftoken']
    print("Code: " + str(response.status_code))
    print(str(r_json))

def mist_GetAPIToken(mist_session):
    print("Getting API Token")
    mist_token_url = "/api/v1/self/apitokens"
    req_url = "https://" + mist_base_url + mist_token_url
    mist_session.get(req_url)
    csrftoken = mist_session.cookies['csrftoken']
    login_data = dict(username=mist_email, password=mist_password, csrfmiddlewaretoken=csrftoken, next='/')
    response = mist_session.post(req_url, data=login_data, headers=dict(Referer=req_url))
    r_json = response.json()
    print("Code: " + str(response.status_code))
    print(str(r_json))
    print(str(r_json["key"]))
    auth_key = str(r_json["key"])
    return auth_key

def mist_GetOrgs(mist_auth_token):
    mist_session = requests.Session()
    print("Getting Orgs for User")
    mist_self_url = "/api/v1/self"
    response = mist_Get(mist_self_url, mist_auth_token)
    myOrgs = []
    if response.status_code == 200:
        print(response.json())
        for org in response.json()['privileges']:
            print(str(org))
            print(str(org['scope']))
            if org['scope'] == 'org':
                print(str(org['org_id']))
                myOrgs.append(str(org['org_id']))
    else:
        print("Error, status code" + str(response.status_code))
    return myOrgs

def CreateRFTemplate(org_id, template_name, country_code, template_24, template_5):
    payload = {
    "name":template_name,
    "country_code":country_code,
    "band_24":{
    "disabled":template_24.disabled,
    "channels":template_24.channels,
    "bandwidth":template_24.bandwidth,
    "power":template_24.power_dbm},
    "band_5":{
    "disabled":template_5.disabled,
    "channels":template_5.channels,
    "bandwidth":template_5.bandwidth,
    "power":template_5.power_dbm},
    }
    mist_rf_template_url = "/api/v1/orgs/" + org_id + "/rftemplates"
    req_url = mist_base_url + mist_rf_template_url
    
    return template_ID

def mist_GetSites(org_id, mist_auth_token):
    mist_site_url = "/api/v1/orgs/" + org_id + "/sites"
    response = mist_Get(mist_site_url, mist_auth_token)
    sites = []
    if response.status_code == 200:
        print(str(response.json()))
    else:
        print("Error: " + respons.status_code)
        print("Message: " + response.json())

def mist_Post(mist_url_extention, payload, mist_auth_token):
    mist_session = requests.Session()
    mist_req_url = "https://" + mist_base_url + mist_url_extention
    header = {"content-type": "application/json", 'Authorization': 'token {}'.format(mist_auth_token)}
    response = mist_session.post(mist_req_url, headers=header, data=json.dumps(payload), verify=False)
    return response

def mist_Get(mist_url_extention, mist_auth_token):
    mist_session = requests.Session()
    mist_req_url = "https://" + mist_base_url + mist_url_extention
    header = {"content-type": "application/json", 'Authorization': 'token {}'.format(mist_auth_token)}
    response = mist_session.get(mist_req_url, headers=header, verify=False)
    return response

def mist_CreateWlanFromTemplate(site_id, Template, mist_auth_token):
    mist_wlan_url = "/api/v1/sites/" + site_id + "/wlans/"
    header = {"content-type": "application/json", 'Authorization': 'token {}'.format(mist_auth_token)}
    response = mist_Post(mist_wlan_url, Template, mist_auth_token)
    wlan_id = ""
    if response.status_code == 200:
        print(str(response.json()))
        wlan_id = response.json()['id']
    else:
        print("Error: " + respons.status_code)
        print("Message: " + response.json())
    return wlan_id


def main(argv):
    global mist_auth_token
    requests.packages.urllib3.disable_warnings()
    mySession = requests.Session()
    if mist_auth_token == "":
        loggedIn = False
        loggedIn = mist_CheckIfLoggedIn(mySession)
        while loggedIn == False:
            mist_LogInCredentials(mySession)
            loggedIn = mist_CheckIfLoggedIn(mySession)
        mist_auth_token = mist_GetAPIToken(mySession)
    mist_CheckIfLoggedInToken(mist_auth_token)
    myOrgs = mist_GetOrgs(mist_auth_token)
    my24temp = bandTemplate(24, 20, [1,6,11], 14, False)
    my5temp = bandTemplate(5, 40, [36,40,44,48,149,153,157,161], 14, False)
    org = "e19cf7f6-af71-46b6-ac79-486e34e42296"
    sites = mist_GetSites(org, mist_auth_token)
    print()
            
        



if __name__ == "__main__":
       main(sys.argv[1:])

