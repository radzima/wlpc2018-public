#!/usr/bin/python
import sys

if not len(sys.argv) == 3:
     print('Usage: script <inputfilename> <outputfilename> ')
     print('inputfilename is anticipating a WLC show run-config output.')
     print('This script replaces the Base Radio Mac Address with the AP for improved readibility')
     print('This script was written by Jake Snyder @jsnyder81.')
     
else:
    myFile = open(sys.argv[1], 'rt')
    myOutput = open(sys.argv[2], 'w')
    myLines = myFile.readlines()

    myAPLibrary = []
    myAP = ""
    myBSSID = ""

    for line in myLines:
        if line.startswith('Cisco AP Name') == True:
            myAP = line.split()[3]
        if line.startswith('      BSSID') == True:
            mySplit = line.split()[2]
            if myBSSID == mySplit:
                continue
            else:
                myBSSID = line.split()[2]
                print(myAP)
                print(myBSSID)
                temp = (myAP, myBSSID)
                myAPLibrary.append(temp)
                continue
    for line in myLines:
        myLine = line
        for AP in myAPLibrary:
            if AP[1] in line:
                myLine = line.replace(AP[1], AP[0])
                print(myLine)
        myOutput.write(myLine)
            
            
            
